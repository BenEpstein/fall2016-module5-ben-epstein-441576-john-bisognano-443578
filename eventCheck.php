<?php
require 'database.php';
ini_set("session.cookie_httponly", 1);
session_start();
if(isset($_SESSION['user'])) {
    $userName = $_SESSION['user'];
    if (isset($_GET['d']) && isset($_GET['m']) && isset($_GET['y'])) {
        $d = 1;
        $m = $_GET['m'];
        if($m<10){$m="0".$m;}
        $y = $_GET['y'];
        $arr = array();
        for($i = 0; $i < 32; $i++) {
            if($d<10){$d="0".$d;}
            $date = ("" . $y . "-" . $m . "-" . $d);
//        echo($userName);
//        echo($date);
            $stmt = $mysqli->prepare("SELECT count(*) FROM events WHERE userName=? AND date=?");
            if (!empty($stmt)) {
                $stmt->bind_param('ss', $userName, $date);
                $stmt->execute();
                $stmt->bind_result($cnt);
                $stmt->fetch();
                if($cnt>0){
                    array_push($arr, 1);
                }
                else{array_push($arr, 0);}

            }
            $d++;
            $stmt->close();
        }
        print(json_encode($arr));
    }
}
else{
    echo "";
}
?>