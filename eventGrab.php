<?php
require 'database.php';
session_start();
if(isset($_SESSION['user'])) {
    $userName = $_SESSION['user'];
    if (isset($_GET['d']) && isset($_GET['m']) && isset($_GET['y'])) {
        $d = $_GET['d'];
        if($d<10){$d="0".$d;}
        $m = $_GET['m'];
        if($m<10){$m="0".$m;}
        $y = $_GET['y'];
        $date = ("".$y."-".$m."-".$d);
//        echo($date);
        $stmt = $mysqli->prepare("SELECT date, time, description, category FROM events WHERE userName=? AND date=? ORDER BY date, time");
        if (!empty($stmt)) {
            $stmt->bind_param('ss', $userName, $date);
            $stmt->execute();
            $stmt->bind_result($date, $time, $description, $category);
            $allEvents = array();
            while ($stmt->fetch()) {
                $arr = array('username' => $userName, 'date'=>$date, 'time' => $time, 'description' => $description, 'category' => $category);
                array_push($allEvents, $arr);
            }
            echo(json_encode($allEvents));
            $stmt->close();
        }

    }
}
else{
    echo "";
}
?>