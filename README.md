#AJAX Calendar

Build a simple calendar that allows users to add and remove events dynamically.
You will use JavaScript to process user interactions at the web browser, without ever refreshing the browser after the initial web page load. You may use a JavaScript library of your choice, including Ext JS.
Your application should utilize AJAX to run server-side scripts that query your database to save and retrieve information, including user accounts and events.
We have also written some functions to help you get started: JavaScript Calendar Library

**Examples**

[https://calendar.google.com/](https://calendar.google.com/)

[http://www.zoho.com/calendar/](http://www.zoho.com/calendar/)

##Server Side Language

Additionally, you may use a database of your choice. MySQL is probably the logical choice since you have been using it since Module 2, but if you're up for a challenge we encourage you to look at other database options like MongoDB.

##Requirements
* Support a month-by-month view of the calendar.
* Show one month at a time, with buttons to move forward or backward.
* There should be no limit to how far forward or backward the user can go.
* Users can register and log in to the website.
* You may leverage your MySQL project code and database from module 3 to get started.
* Unregistered users should see no events on the calendar.
* Registered users can add events.
* All events should have a date and time, but do not need to have a duration.
* You do not need to support recurring events (where you add an event that repeats, for example, every monday).
* Registered users see only events that they have added.
* Your AJAX should not ask the server for events from a certain username.
* Instead, your AJAX should ask the server for events, and the server should respond with the events for only the currently-logged-in user (from the session). Can you think of why?
* Registered users can delete their events, but not the events of others.
* Again, the server should delete events based on the username present in the session. (If it deletes only based on an Event ID, an attacker could feed any arbitrary Event ID to your server, even if he/she didn't own that event.)
* All user and event data should be kept in a database.
* ***At no time should the main page need to be reloaded.***
* User registration, user authentication, event addition, and event deletion should all be handled by JavaScript and AJAX requests to your server.
* Your page needs to work in the versions of Firefox and Chrome installed on the lab computers.
* Tip: Run your database schema by a TA before implementing it.

***Web Security and Validation***

* Your project needs to demonstrate that thought was put into web security and best practice. For more information, see this week's Web Application Security guide: Web Application Security, Part 3

**In particular:** 

* Your application needs to prevent XSS attacks. Be careful when transmitting data over JSON that will be reflected in an event title! (Note: JSON data should be sanitized on the client side, not the server side.)
* Perform precautionary measures to prevent session hijacking attacks.
* You should specify your session cookie to be HTTP-Only. However, for the means of this module, you need not test for user agent consistency.
* Optional: Validate your JavaScript code using JSHint or JSLint for up to 3 points of extra credit.
* You should continue the practices that you have learned in past weeks:
* Pass tokens in forms to prevent CSRF attacks.
* Hint: You will need to send your CSRF tokens in your AJAX requests. Remember that AJAX still submits forms and runs server-side scripts, just like the vanilla forms you've been using in Modules 2 and 3.
* Use prepared queries to prevent SQL Injection attacks.
* If storing passwords in a database, always store them salted and encrypted.
Your page should validate with no errors through the W3C validator.


***Creative:***

We are able to create an event and share it with a other users.


***(No longer active, EC2 instance terminated at end of course)***
http://ec2-54-172-15-250.compute-1.amazonaws.com/~BEpstein/module_5/group/calendar.php
	
	Login info: (username and password)
	benepstein
	password
	
	johnbo
	password