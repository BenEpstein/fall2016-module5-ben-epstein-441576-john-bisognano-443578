<!DOCTYPE html>
<html lang="en">
<head>
    <link type="text/css" rel="stylesheet" href="StyleCal.css"/>
    <meta charset="UTF-8">
    <title>Calendar</title>
    <script type="text/javascript" src="jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="jquery.leanModal.min.js"></script>
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css"/>
    <?php
    ini_set("session.cookie_httponly", 1);
    session_start();
    //    echo($_SESSION['user']);
    //    $_SESSION['token'] = substr(md5(rand()), 0, 10);
    //    echo($_SESSION['token']);
    ?>
    <script>

        function events(curDay, curMonth, curYear) {
            curMonth++;

            if (window.XMLHttpRequest) {
                xmlhttp = new XMLHttpRequest();
            } else {
                xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    formatInfo(xmlhttp.responseText, curDay, curMonth, curYear);
//                    alert(xmlhttp.responseText);

                }
            };
            xmlhttp.open('GET', 'eventGrab.php?d=' + curDay + '&m=' + curMonth + '&y=' + curYear, true);
            xmlhttp.send();

        }


        function checkEvent(curDay, curMonth, curYear) {
            curMonth++;
//            alert(curDay);
//            alert(curMonth);
//            alert(curYear);
//            while (curDay <= 31) {
            if (window.XMLHttpRequest) {
                xmlhttp = new XMLHttpRequest();
            } else {
                xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
            }

            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    var arrayDays = JSON.parse(xmlhttp.responseText);
                    for (var e = 0; e < arrayDays.length; e++) {
                        if (arrayDays[e] == 1) {
                            document.getElementsByClassName("currentmonth")[e].innerHTML = ""+(e+1)+"!";
                        }
                        else{
                            document.getElementsByClassName("currentmonth")[e].innerHTML = (e+1);
                        }
                    }

                }
            };

            xmlhttp.open('GET', 'eventCheck.php?d=' + curDay + '&m=' + curMonth + '&y=' + curYear, true);
            xmlhttp.send();
        }
        function deleteEvent(curDay, curMonth, curYear) {
//            alert("here");
            var today=curDay;
            if (curMonth < 10) {
                curMonth = "0" + curMonth;
            }
            if (curDay < 10) {
                curDay = "0" + curDay;
            }
            var date = ("" + curYear + "-" + curMonth + "-" + curDay);
            var time = $("")
            var info = {"date": date};
            $.ajax({
                type: "POST",
                url: "deleteEvent.php",
                data: info,
                success: function (answer) {
                    if ($.trim(answer) === "Deleted") {
                        alert("Event Deleted");
                        $("#events").html("");
//                        document.getElementsByClassName("currentmonth")[curDay-1].innerHTML = today;
                        checkEvent(curDay, curMonth-1, curYear);
                    } else {
                        alert(answer);
                    }
                }
            });
        }


        function formatInfo(response, curDay, curMonth, curYear) {
            if (response === "") {
                alert("Not logged in");
            }
            $("#events").html(response);
            var arr = JSON.parse(response);
            var i;
            var out = "<table>";

            for (i = 0; i < arr.length; i++) {
                if (arr[i].category === null) {
                    arr[i].category = "";
                }
                var time = arr[i].time;
                out += "<tr><td>" +
                    arr[i].username +
                    "</td><td>" +
                    arr[i].date +
                    "</td><td>" +
                    arr[i].time +
                    "</td><td id="+arr[i].time+" onclick= deleteEvent(" + curDay + ',' + curMonth + ',' + curYear + ")>" + arr[i].description +
                    "</td><td>" +
                    arr[i].category +
                    "</td>" +
                    "</tr>";
            }
            out += "</table>";
            document.getElementById("events").innerHTML = out;

        }
    </script>
</head>
<body>
<!--Taken from andwecode.com for the login popup anamation-->
<div class="container">
    <a id="modal_trigger" href="#modal" class="btn">Login/Register</a>

    <div id="modal" class="popupContainer" style="display:none;">
        <header class="popupHeader">
            <span class="header_title">Login</span>
            <span class="modal_close"><i class="fa fa-times"></i></span>
        </header>

        <div class="popupBody">
            <!-- Social Login -->
            <div class="social_login">
                <div class="centeredText">
                    <span>Log in or Sign up</span>
                </div>

                <div class="action_btns">
                    <div class="one_half"><a href="#" id="login_form" class="btn">Login</a></div>
                    <div class="one_half last"><a href="#" id="register_form" class="btn">Sign up</a></div>
                </div>
            </div>

            <!-- Username & Password Login form -->
            <div class="user_login">
                <form>
                    <label>Username</label>
                    <input type="text" id="usernameLogin" required/>
                    <br/>

                    <label>Password</label>
                    <input type="password" id="password" required/>
                    <br/>
                    <input type="hidden" id="token1" name="token1" value="<?php echo $_SESSION['token']; ?>"/>

                    <div class="action_btns">
                        <div class="one_half"><a href="#" class="btn back_btn"><i class="fa fa-angle-double-left"></i>
                                Back</a></div>
                        <div class="one_half last"><a href="#" class="btn btn_red" id="login">Login</a>
                        </div>
                    </div>
                </form>
            </div>

            <!-- Register Form -->
            <div class="user_register">
                <form>
                    <label>Full Name</label>
                    <input type="text" id="FullNameReg" required/>
                    <br/>

                    <label>User name</label>
                    <input type="text" id="UserNameReg" required/>
                    <br/>

                    <label>Email Address</label>
                    <input type="email" id="EmailReg" required/>
                    <br/>

                    <label>Password</label>
                    <input type="password" id="PassReg" required/>
                    <br/>
                    <input type="hidden" id="token2" name="token2" value="<?php echo $_SESSION['token']; ?>"/>

                    <div class="action_btns">
                        <div class="one_half"><a href="#" class="btn back_btn"><i class="fa fa-angle-double-left"></i>
                                Back</a></div>
                        <div class="one_half last"><a href="#" class="btn btn_red" id="register">Register</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <a id="modal1_trigger" href="#modal1" class="btn">Create Event</a>

    <div id="modal1" class="popupContainer" style="display:none;">
        <header class="popupHeader">
            <span class="header_title">Event</span>
            <span class="modal_close"><i class="fa fa-times"></i></span>
        </header>

            <!-- Event info -->
            <!--            <div class="event_info">-->
            <!--                <div class="centeredText">-->
            <!--                    <span>Create Event</span>-->
            <!--                </div>-->
            <!---->
            <!--                <div class="action_btns">-->
            <!--                    <div class="one_half"><a href="#" id="login_form" class="btn">Create Event</a></div>-->
            <!--                    <div class="one_half last"><a href="#" id="register_form" class="btn">Edit Event</a></div>-->
            <!--                </div>-->
            <!--            </div>-->

            <div class="popupBody">
                <div class="create_event">
                    <form>
                        Event name<input type="text" id="eventName" required/>
                        <br/>
                        Date<input type="date" id="date" required/>
                        <br/>
                        Time<input type="time" id="time" required/>
                        <br/>
                        Share?<input type="text" id="shared"/>
                        <br/>
                        <input type="hidden" id="token" name="token" value="<?php echo $_SESSION['token']; ?>"/>

                        <div class="action_btns">
                            <div class="one_half last"><a href="#" class="btn btn_red" id="create">Create
                                    Event</a></div>
                        </div>
                    </form>
                </div>

            </div>
    </div>


</div>

<!------------------------------------------------------------------------------------------------------------------------------------>

<!--Down to here, some functions edited from andwecode.com-->

<button id="prev" >prev</button>
<button id="next" >next</button>
<div id="calendar"></div>
<input type="submit" name="logout" value="Log Out" id="logout"/>
<table id="eventTable">
    <tr>
    <td id="events"></td>
    </tr>
</table>
<button id="today"> Today</button>
<br><br><br>

<script type="text/javascript">


    (function () {
        var current = new Date();
        var cmonth = current.getMonth(); // current (today) month
        var disMonth = cmonth; // this is the month we will display on the screen
        var day = current.getDate();
        var year = current.getFullYear();
        var disYear = year;

//        -----------------------------------Modals start here------------------------------------------------------------------------

        $("#logout").click(function () {
            $.ajax({
                type: "POST",
                url: "logout.php",
                data: "",
                success: function (answer) {
                    if (answer == "success") {
                        alert("Logged out");
                        go();
                        $("#events").html("");

                    } else {
                        alert(answer);
                    }
                }
            });
        });


        var curUser = "";
        $("#modal_trigger").leanModal({top: 200, overlay: 0.5, closeButton: ".modal_close"});

        $(function () {
            // Calling Login Form
            $("#login_form").click(function () {
                $(".social_login").hide();
                $(".user_login").show();
                return false;
            });

            $("#login").click(function () {

                var pass = $("#password").val();
                var userLogin = $("#usernameLogin").val();
                var token1 = $("#token1").val();
                var logindict = {'userLog': userLogin, "passLog": pass, "token": token1};
                $.ajax({
                    type: "POST",
                    url: "info.php",
                    data: logindict,
                    success: function (answer) {
                        var arr = JSON.parse(answer);
                        if ($.trim(arr[0]) == "failure") {
                            alert("incorrect Username or Password");
                        }
                        else if ($.trim(arr[0]) == "token") {
                            alert("Request forgery detected");
                        }
                        else {
                            curUser = arr[0];
                            alert("Logged in as " + arr[0]);
                            var toke = arr[1];
                            document.getElementById('token').setAttribute("value", toke);
                            document.getElementById('token1').setAttribute("value", toke);
                            document.getElementById('token2').setAttribute("value", toke);
                            go();

                        }
                    }
                });
            });
            $("#register").click(function () {
                var FullNameReg = $("#FullNameReg").val();
                var UserNameReg = $("#UserNameReg").val();
                var EmailReg = $("#EmailReg").val();
                var PassReg = $("#PassReg").val();
                var token2 = $("#token2").val();
                var registerdict = {
                    "name": FullNameReg,
                    "user": UserNameReg,
                    "email": EmailReg,
                    "pass": PassReg,
                    "token": token2
                };
                $.ajax({
                    type: "POST",
                    url: "register.php",
                    data: registerdict,
                    success: function (answer) {
                        var arr = JSON.parse(answer);
                        if ($.trim(arr[0]) == "win") {
                            alert("success");
                            curUser = arr[2];
                            alert("Logged in as " + arr[2]);
                            var toke = arr[1];
                            document.getElementById('token').setAttribute("value", toke);
                            document.getElementById('token1').setAttribute("value", toke);
                            document.getElementById('token2').setAttribute("value", toke);
                            go();
                            $(".modal_close");
                        }
                        else if ($.trim(answer) == "token") {
                            alert("Request forgery detected");
                        }
                        else {
                            alert("Failed");

                        }
                    }
//                dataType: dataType
                });
            });


            // Calling Register Form
            $("#register_form").click(function () {
                $(".social_login").hide();
                $(".user_register").show();
                $(".header_title").text('Register');
                return false;
            });

            // Going back to Social Forms
            $(".back_btn").click(function () {
                $(".user_login").hide();
                $(".user_register").hide();
                $(".social_login").show();
                $(".header_title").text('Login');
                return false;
            });

        });

        $(function () {
            // Calling Event Form
            $("#modal1_trigger").click(function () {
                $(".create_event").show();
                return false;
            });

            $("#create").click(function () {
                var eventName = $("#eventName").val();
                var date = $("#date").val();
                var time = $("#time").val();
                var shared = $("#shared").val();
                var token = $("#token").val();
                if (eventName === "" || date === "" || time === "") {
                    alert("Fill out all forms");
                }
                else {
                    var registerdict = {
                        "eventName": eventName,
                        "date": date,
                        "time": time,
                        "share": shared,
                        "token": token
                    };
                    $.ajax({
                        type: "POST",
                        url: "createEvent.php",
                        data: registerdict,
                        success: function (val) {
                            if ($.trim(val) === "Event Created") {
                                alert("Your event is live");
                                go();

                            }
                            else if ($.trim(val) === "Log") {
                                alert("Not logged in");
                            }
                            else if ($.trim(val) === "token") {
                                alert("Forgery request detected");
                            }
                            else {
                                alert(val);
                            }
                        }
                    });
                }

            });
        });
        $("#modal1_trigger").leanModal({top: 200, overlay: 0.5, closeButton: ".modal_close"});

//        -----------------------------------Modals end here------------------------------------------------------------------------


        function calendar(month) {
            //Variables to be used later.  Place holders right now.
            var padding = "";
            var totalFeb = "";
            var i = 1;
            var testing = "";
            var tempMonth = month + 1; //+1; //Used to match up the current month with the correct start date.
            var prevMonth = month - 1;

            //Determing if Feb has 28 or 29 days in it.
            if (month == 1) {
                if ((year % 100 !== 0) && (year % 4 === 0) || (year % 400 === 0)) {
                    totalFeb = 29;
                } else {
                    totalFeb = 28;
                }
            }

            // Setting up arrays for the name of the months, days, and the number of days in the month.
            var monthNames = ["Jan", "Feb", "March", "April", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"];
            var totalDays = ["31", "" + totalFeb + "", "31", "30", "31", "30", "31", "31", "30", "31", "30", "31"];

            // Temp values to get the number of days in current month, and previous month. Also getting the day of the week.
            var tempDate = new Date(tempMonth + ' 1 ,' + year); //gets the start day of the month
            var tempweekday = tempDate.getDay();
            var tempweekday2 = tempweekday; //for incrementing and filling in the calendar days
            var dayAmount = totalDays[month];

            // After getting the first day of the week for the month, padding the other days for that week with the previous months days.  IE, if the first day of the week is on a Thursday, then this fills in Sun - Wed with the last months dates, counting down from the last day on Wed, until Sunday.
            while (tempweekday > 0) {
                padding += "<td class='premonth'></td>";
                tempweekday--;
            }
            // Filling in the calendar with the current month days in the correct location along.
            while (i <= dayAmount) {
                var dayofMonth = 1;
                // Determining when to start a new row
                if (tempweekday2 > 6) {
                    tempweekday2 = 0;
                    padding += "</tr><tr>";
                }

                // checking to see if i is equal to the current day, if so then we are making the color of that cell a different color using CSS. Also adding a rollover effect to highlight the day the user rolls over. This loop creates the actual calendar that is displayed.
                if (i == day && month == cmonth && year == current.getFullYear()) {

                    padding += "<td id='currentday' class='currentmonth'   onclick= events(" + i + ',' + month + ',' + year + ")  >" + i + "</td>";
                } else {
                    padding += "<td class='currentmonth' onclick= events(" + i + ',' + month + ',' + year + ")  >" + i + "</td>";

                }
                tempweekday2++;
                i++;
            }


            var calendarTable = "<table class='calendar'> <tr class='currentmonths'><th colspan='7'>" + monthNames[month] + " " + year + "</th></tr>";
            calendarTable += "<tr class='weekdays'>  <td>Sun</td>  <td>Mon</td> <td>Tues</td> <td>Wed</td> <td>Thurs</td> <td>Fri</td> <td>Sat</td> </tr>";
            calendarTable += "<tr>";
            calendarTable += padding;
            calendarTable += "</tr></table>";
            document.getElementById("calendar").innerHTML = calendarTable;
        }


        function go() {
            checkEvent(1, disMonth, year);
            calendar(disMonth);
        }

        function goPrev() {
            disMonth--;
            if (disMonth == -1) {
                disYear--;
                year = disYear;
                disMonth = 11;
            }
            checkEvent(1, disMonth, year);
            $("#events").html("");
            calendar(disMonth);
        }

        function goNext() {
            disMonth++;
            if (disMonth == 12) {
                disYear++;
                year = disYear;
                disMonth = 0;
            }
            checkEvent(1, disMonth, year);
            $("#events").html("");
            calendar(disMonth);
        }

        function today() {
            cmonth = current.getMonth();
            disMonth = cmonth;
            year = current.getFullYear();
            disYear = year;
            checkEvent(1, cmonth, year);
            calendar(cmonth);
        }

        document.addEventListener("DOMContentLoaded", go, false);
        document.getElementById("prev").addEventListener('click', goPrev, false);
        document.getElementById("next").addEventListener('click', goNext, false);
        document.getElementById("today").addEventListener('click', today, false);
        document.addEventListener("DOMContentLoaded", checkEvent(1, cmonth, year), false);


    })();

</script>


</body>
</html>


