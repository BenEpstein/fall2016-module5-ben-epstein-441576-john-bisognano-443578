<?php
require 'database.php';
ini_set("session.cookie_httponly", 1);
session_start();
if (isset($_POST)) {
    $user = $_POST['userLog'];
    $stmt = $mysqli->prepare("SELECT COUNT(*), password FROM users WHERE userName=?");
// Bind the parameter
    $stmt->bind_param('s', $user);
    $stmt->execute();

// Bind the results
    $stmt->bind_result($cnt, $pwd_hash);
    $stmt->fetch();

    $pwd_guess = $_POST['passLog'];
// Compare the submitted password to the actual password hash
    if ($cnt == 1 && crypt($pwd_guess, $pwd_hash) == $pwd_hash) {
        // Login succeeded!
        $_SESSION['user'] = $user;
        $_SESSION['token'] = substr(md5(rand()), 0, 10);
        $arr = array();
        array_push($arr, $user);
        array_push($arr, $_SESSION['token']);
        print(json_encode($arr));
    } else {
        echo "failure";
//        echo(crypt($pwd_guess)); echo("<br>");
//        echo($pwd_hash);
    }
}
else{
    echo("token");
}

//elseif ($_POST['submit'] == 'Register!') {
//    header("Location: register.php");
//}